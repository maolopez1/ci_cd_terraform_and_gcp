# CI_CD_terraform_and_gcp

Terraform templates to create VPC infrastructure in GCP, GitLab runners, Container Registry, etc.


## Steps:


### 1.- in GCP create an instance as following

  A.-  Enable HTTP/S

  B.-  Enable Ephemeral Public IP

  C.-  Access to all APIs

  D.- Create a terraform-deploy Service Agent with role Owner, copy the keys.

### 2.- Install the runner in the instance

  A.-  gitlab-runner register , gitlab-runner status

  B.-  As root add in the config.toml the following:


       [[runners]]
         executor = "docker"
         [runners.docker]
           privileged = true

  C.-  gitlab-runner restart, gitlab-runner status


## On GitLab Side:


  1.- Create a Service Account for Terraform with role Owner and get the keys.

  2.- In the pipeline create a variable SERVICEACCOUNT and paste the keys in there.

  3.- Replace your GCP Project-ID in the terraform variable files.

  4.- In the pipeline create a variable REGISTRATIONTOKEN and paste the Gitlab runner
      registration token found in: Settings -> CI/CD -> Runners -> Set up a specific 
      runner manually.


## Expected Results

Below a succesful Gitlab Runner registration:


[root@anagramma-northamerica-northeast1-instance1 /]# ./startup.sh
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  6904  100  6904    0     0  13662      0 --:--:-- --:--:-- --:--:-- 13671
Importing GPG key 0x51312F3F:
 Userid     : "GitLab B.V. (package repository signing key) <packages@gitlab.com>"
 Fingerprint: f640 3f65 44a3 8863 daa0 b6e0 3f01 618a 5131 2f3f
 From       : https://packages.gitlab.com/runner/gitlab-runner/gpgkey
Importing GPG key 0x51312F3F:
 Userid     : "GitLab B.V. (package repository signing key) <packages@gitlab.com>"
 Fingerprint: f640 3f65 44a3 8863 daa0 b6e0 3f01 618a 5131 2f3f
 From       : https://packages.gitlab.com/runner/gitlab-runner/gpgkey
warning: /var/cache/yum/x86_64/7/runner_gitlab-runner/packages/gitlab-runner-13.11.0-1.x86_64.rpm: Header V4 RSA/SHA512 Signature, key ID 880721d4: NOKEY
Importing GPG key 0x51312F3F:
 Userid     : "GitLab B.V. (package repository signing key) <packages@gitlab.com>"
 Fingerprint: f640 3f65 44a3 8863 daa0 b6e0 3f01 618a 5131 2f3f
 From       : https://packages.gitlab.com/runner/gitlab-runner/gpgkey
Importing GPG key 0x880721D4:
 Userid     : "GitLab, Inc. <support@gitlab.com>"
 Fingerprint: 3018 3ac2 c4e2 3a40 9efb e705 9ce4 5abc 8807 21d4
 From       : https://packages.gitlab.com/runner/gitlab-runner/gpgkey/runner-gitlab-runner-366915F31B487241.pub.gpg
Runtime platform                                    arch=amd64 os=linux pid=20786 revision=7f7a4bb0 version=13.11.0
Running in system-mode.

Registering runner... succeeded                     runner=hxyUj9Pz
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
[root@anagramma-northamerica-northeast1-instance1 /]#



