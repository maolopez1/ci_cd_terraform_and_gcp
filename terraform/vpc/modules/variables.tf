variable "project" { 
  default = "playground-s-11-e8434525"
}

variable "region" {
  default = "northamerica-northeast1" 
}

variable "zonea" {
  default = "northamerica-northeast1-a" 
}

variable "zoneb" {
  default = "northamerica-northeast1-b" 
}

variable "zonec" {
  default = "northamerica-northeast1-c" 
}

variable "cidr" {
  default = "10.0.0.0/16" 
}

variable "subnet1" {
  default = "10.162.128.0/20"
}

variable "subnet2" {
  default = "10.162.64.0/20"
}

variable "subnet3" {
  default = "10.163.128.0/20"
}

variable "subnet4" {
  default = "10.163.64.0/20"
}

variable "machine_type" { 
  default = "e2-standard-2"
}

variable "image" { 
  default = "centos-cloud/centos-7-v20200403"
}

variable "email" { 
  default = "terraform-deployments@playground-s-11-e8434525.iam.gserviceaccount.com"
}

variable "company" { 
  default = "anagramma"
}

