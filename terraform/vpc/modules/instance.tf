resource "google_compute_instance" "default" {
  name         = "${var.company}-${var.region}-instance1"
  metadata_startup_script = file("./startup.sh")
  machine_type  =   var.machine_type
  zone          =   var.zoneb
  tags          =   ["ssh","http"]
  boot_disk {
    initialize_params {
      image     =   var.image
    }
  }

  network_interface {
    network = google_compute_network.vpc.name
    subnetwork = google_compute_subnetwork.subnetwork1.name
    access_config {
      // Ephemeral IP
    }
  }
}
