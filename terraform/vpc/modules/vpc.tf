terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.65.0"
    }
  }
}

locals {
  project  = var.project
  region   = var.region
  enabled_google_api = toset([
    "compute.googleapis.com",
    "serviceusage.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "containerregistry.googleapis.com"])
}

resource "google_project_service" "enable_googleapis" {
  for_each = local.enabled_google_api
  project = var.project
  service = each.value
  disable_dependent_services = false
  disable_on_destroy = false
}

resource "google_container_registry" "registry" {
  location = "US"
}

resource "google_compute_network" "vpc" {
  name          =  "${var.company}-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnetwork1" {
   name          = "${var.company}-subnetwork1"
   ip_cidr_range = var.subnet1
   region        = var.region
   network       = google_compute_network.vpc.id
}

resource "google_compute_firewall" "allow-internal" {
  name    = "fw-allow-internal"
  network = google_compute_network.vpc.name
  allow {
  protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["1-65535"]
  }

  allow {
    protocol = "udp"
    ports    = ["1-65535"]
  }
  source_ranges = ["10.0.0.0" ]
}

resource "google_compute_firewall" "allow-http" {
  name    = "fw-allow-http"
  network = google_compute_network.vpc.name

allow {
    protocol = "tcp"
    ports    = ["80", "443", "7080", "8082"]
  }
  target_tags = ["http"]
}

resource "google_compute_firewall" "allow-bastion" {
  name    = "fw-allow-bastion"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  target_tags = ["ssh"]
  }
