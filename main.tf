provider "google" {
    credentials = file("./creds/serviceaccount.json")
    project     = var.project
    region      = var.region
}

provider "google-beta" {
    credentials = file("./creds/serviceaccount.json")
    project     = var.project
    region      = var.region
}

module "vpc" {
  source = "./terraform/vpc/modules"
}

module "instance" {
  source = "./terraform/vpc/modules"
}

