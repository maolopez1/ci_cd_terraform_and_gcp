#!/bin/bash -x


yum upgrade -y > tmp/1-yum.txt
yum install docker -y > /tmp/2-docker.txt
systemctl enable docker
systemctl start docker
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash > tmp/3-curl.txt
yum install gitlab-runner -y > tmp/4-runner.txt
chmod u+w /etc/sudoers
sed -i '$ a gitlab-runner ALL=(ALL) NOPASSWD: ALL' /etc/sudoers
chmod u-w /etc/sudoers
gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token ${TOKEN} \
  --executor docker \
  --description "docker-builder" \
  --docker-image "docker:latest" \
  --docker-privileged

#Your credentials are saved in your user home directory: Linux: $HOME/.docker/config.json
