#!/bin/bash -x

pwd
mkdir -p ./creds
echo $SERVICEACCOUNT | base64 -d > ./creds/serviceaccount.json
export PROJECT=`cat ./creds/serviceaccount.json | grep project_id | awk -F '"' '{print $4}'`
gcloud config set project ${PROJECT}
gcloud auth activate-service-account --key-file=./creds/serviceaccount.json

